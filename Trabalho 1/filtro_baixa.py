import cv2 as cv
import numpy as np

def filter(img, func, lim) :
    filtered = img.copy()
    y = img.shape[0]
    x = img.shape[1]

    for i in range(1, y - 1) :
        for j in range(1, x - 1) :
            for k in range(3) :
                w = filtered[(i - 1): (i + 2), (j - 1): (j + 2), k]
                if( abs(filtered[i, j, k] - w) > lim).any() :
                    filtered[i, j, k] = func(np.concatenate(w))
    return filtered
