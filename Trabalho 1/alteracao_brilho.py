import cv2 as cv

img = cv.imread('canyon.jpg')
y = img.shape[0]
x = img.shape[1]

for i in range(y) :
	for j in range(x) :
		for k in range(3) :
			b = img[i, j, k] + 100
			if b > 255 :
				img[i, j, k] = 255
            elif b < 0 :
                img[i, j, k] = 0
			else :
				img[i, j, k] = b

cv.imwrite("brilhosa.jpg", img)
