import numpy as np
import random
import cv2

def pepper_noise(img, tax) :
    count = 0
    k = int(img.shape[1] * img.shape[0] * tax)
    noise = img.copy()
    while(k > count):
            j = random.randint(0, img.shape[1] - 1)
            i = random.randint(0, img.shape[0] - 1)
            if (noise[i][j] != 0).all():
                noise[i][j] = 0
                count += 1
    return noise
