import cv2 as cv

img = cv.imread('gato.png', 0)
cv.imshow('imagem', img) # Mostra a imagem em uma janela

print(img.shape) # Retorna o número de linhas, colunas e bandas da imagem.
print(img.size) # Retorna o número total de pixels
